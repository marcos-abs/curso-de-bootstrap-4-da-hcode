# Modal Centered

## Define a posicionamento central para o modal

tags: #center, #centered, #modal

### fonte: [modal-centered.html](modal-centered.html)

```html
<div class="modal-dialog modal-dialog-centered">
```

#### Veja o exemplo

![Modal Centered](2021-09-05-15-41-38.png)
A diferença entre o Modal Centered e o Modal Padrão é a classe em destaque de vermelho, no exemplo acima (modal-dialog-centered).

Retorne para o documento anterior:[[modals]]
