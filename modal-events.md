# Modal Events

## Acompanha dos eventos via javascript antes e depois do modal

tags: #events, #modal

### fonte:[modal-events.html](modal-events.html)

![Modal Events - criando um modal](2021-09-05-16-41-26.png)

A diferença entre o Modal Events e o Modal Padrão são os eventos gerados em destaque no console do DevTools, sendo que ao clicar é disparado o evento "show.bs.modal" (em ciano) e na sequencia ao concluir o ato de mostrar o modal é disparado outro evento "shown.bs.modal" (em vermelho).

---
   Código fonte:

   ```html
        <script>
            $(() => {
                $('#btn-continue').on('click', (e) => {
                    $('#modal-1').modal('hide');
                });
            });
        </script>
   ```

![Modal Events - encerrando o modal](2021-09-05-16-45-31.png)
Agora ao clicar nos botões e encerrar o modal é mostrado (no console do DevTools) mais 2 eventos sendo eles: "hide.bs.modal" (antes de concluir o encerramento do modal - em verde) e o "hidden.bs.modal" (ao concluir completamente o encerrar do modal - em rosa-choque).

Retorne para o documento anterior:[[modals]]
