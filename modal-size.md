# Modal Size

## Faz com que o tamanho do Modal seja sempre o mesmo independente do tamanho do dispositivo

tags: #size, #modal

### fonte: [modal-size.html](modal-size.html)

![Modal Size](2021-09-05-14-43-23.png)
A diferença entre o Modal Size e o Modal Padrão é a classe destacada em vermelho, no exemplo acima (modal-xl).

![Veja o resultado](2021-09-05-14-58-05.png)

Retorne para o documento anterior:[[modals]]
