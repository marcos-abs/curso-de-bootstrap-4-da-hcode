# Modals

## Default (Padrão)

### fonte: [modal.html](modal.html)

Para a criação do modal são necessários os seguintes componentes HTML:

1. Botão de abertura do Modal:
![Botão de abertura do Modal](/assets/img/modal-button-open.png)

   ```html
      <button
         type="button"
         class="btn btn-primary btn-lg m-2"
         data-toggle="modal"
         data-target="#modal-1">
            Abrir
      </button>
   ```

2. Estrutura do Modal:
![Botão de abertura do Modal](/assets/img/modal-structure.png)

   * em vermelho: serão detalhados individualmente;
   * em verde: essa identificação será utilizada pelo botão de abertura (veja botão de abertura do modal);
   * em ciano: o cabeçalho e o rodapé do modal.

   Emmet:

   ```emmet
   .modal>.modal-dialog>.modal-content>.modal-header>.modal-title{Teste do Modal}
   ```

   Código HTML:

   ```html
   <div class="modal" id="modal-1">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <div class="modal-title">Teste do Modal</div>
                  <!-- botão de abertura -->
               </div>
               <div class="modal-body">
                  <!-- Conteúdo do Modal -->
               </div>
               <div class="modal-footer">
                  <!-- botão de cancelamento -->
                  <!-- botão de continuação -->
               </div>
            </div>
         </div>
      </div>
   </div>
   ```

3. Botão "fechar" Modal:
![Botão de abertura do Modal](/assets/img/modal-button-close.png)
   * em vermelho: o detalhamento do botão;
   * em ciano: a classe de configuração da aparencia do botão;
   * em verde: a ação de fechamento do Modal.

   ```html
      <button
         class="close"
         type="button"
         data-dismiss="modal">
         <span>&times;</span>
      </button>
   ```

4. Botões "cancelar" e "continuar" do Modal:
![Botões de cancelar e continuar do Modal](/assets/img/modal-buttons-cancel-continue.png)
   * em vermelho: o detalhamento dos botões;
   * em ciano: a ação de cancelamento do botão específico;
   * em verde: a identificação do Modal para ser utilizada pelo JQuery.

   ---
   Código fonte do botão de cancelar:

   ```html
      <button
         type="button"
         data-dismiss="modal"
         class="btn btn-secondary">
            Cancelar
      </button>
   ```

   ---
   Código fonte do botão de continuar:

   ```html
      <button
         type="button"
         class="btn btn-primary"
         id="btn-continue">
            Continuar
      </button>
   ```

5. Conteúdo do Modal:
![Conteúdo do Modal](/assets/img/modal-conteudo.png)
   * em vermelho: o conteúdo do modal.

6. JQuery do Modal para o botão continuar:
![JQuery do Modal](/assets/img/modal-jquery.png)
   * em ciano: a identificação do botão de acionamento do script jquery;
   * em vermelho: a identificação do seletor e a ação que será executada, caso o botão seja acionado.

   ---
   Código fonte:

   ```html
           <script>
            $(() => {
                $('#btn-continue').on('click', (e) => {
                    $('#modal-1').modal('hide');
                });
            });
        </script>
   ```

7. O resultado (antes de clicar no botão):
![Modal antes](/assets/img/modal-off.png)

8. O resultado (depois de clicar no botão):
![Modal depois](/assets/img/modal-on.png)

Retorne para o documento anterior: [[bootstrap4]]
ou avance para os demais: [[modal-size]],[[modal-scroll]],
[[modal-centered]],[[modal-events]]
