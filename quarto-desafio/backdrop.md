# Backdrop

## Diminui o foco dos demais elementos da tela para que o usuário escolha corretamente as opções disponíveis

tags: #class, #navbar

### fonte: [index.html](index.html)

```html

<div class="backdrop"></div>

```

#### Veja o exemplo

![backdrop](2021-09-07-14-48-57.png)
em destaque (vermelho)
