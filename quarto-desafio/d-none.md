# d-none


## Esconde o bloco de código dentro da tag (util quando queremos mostrar o conteúdo da TAG somente numa resolução específica)

tags: #d-none, #d-lg-block, #class, #bootstrap4, #d-sm-block

### fonte: [index.html](index.html)

```html
<p class="d-none d-lg-block">
```

#### Veja o exemplo

![d-none com d-lg-block](2021-09-08-00-32-17.png)
O exemplo acima mostra o conteúdo do parágrafo somente para dispositivos "lg" (<= 992px)
Retorne para o documento anterior:[[bootstrap4]]
