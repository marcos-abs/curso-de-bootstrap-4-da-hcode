# DevTools

## Como verificar se o Bootstrap foi carregado corretamente

### fonte: [index.html](index.html)

![Bootstrap](2021-09-05-17-17-47.png)
Certifique-se que a linha de "carga" do Bootstrap está correta, utilizando como exemplo o modelo acima (destaque em vermelho). Veja o codigo abaixo:

```html
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
```

![DevTools](2021-09-05-17-20-49.png)
Com o navegador aberto, abra o DevTools (pressione F12) e clique na aba "Network" (destaque em ciano). N a sequencia, pressione F5 para recarregar a página no navegador e se tudo estiver correto aparecerá uma linha como a destaque (em vermelho) com o nome do arquivo do Bootstrap.

#### Como verificar se o favicon está correto

![Favicon](2021-09-05-17-25-46.png)
Com o arquivo ".html" aberto, certifique-se de que a linha de código está como na imagem em destaque acima (em vermelho)

```html
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
```
