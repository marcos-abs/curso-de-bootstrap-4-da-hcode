# no-gutters

## classe do Bootstrap que tira os espaços entre as colunas

tags: #bootstrap, #no-gutters, #class, #alinhamento, #colunas

### fonte:[index.html](index.html)

```html
<div class="row no-gutters">
```

#### Veja o exemplo

![no-gutters](2021-09-07-23-31-59.png)

Retorne para o documento anterior:[[bootstrap4]]
